from django.db import models

# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=50,primary_key=True)
    status = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add = True, null = True)

    def __str__(self):
        return self.name
