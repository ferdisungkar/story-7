from django import forms
from .models import User

class User_Input(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : "What's your name?",
        'type' : 'text',
        'maxlength' : '50',
        'required' : True,
        'label' : '',
    }))
    status = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : "Tell us how are you feeling today",
        'type' : 'text',
        'maxlength' : '50',
        'required' : True,
        'label' : '',
    }))
