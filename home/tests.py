from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .apps import *
from .views import *
from .models import *
from selenium import webdriver
import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse


# Create your tests here.

class StoryUnitTest (TestCase):
    def test_homepage_url_template_and_func(self):
        response = Client().get('')
        found = resolve('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertEqual(found.func, index)

    def test_confirmation_page_url_template_and_func(self):
        response = Client().get('/confirmation/')
        found = resolve('/confirmation/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirmation.html')
        self.assertEqual(found.func, confirmation)

    def test_homepage_contains_content(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Ferdi Sungkar", response_content)
        self.assertIn("Hello!", response_content)
        self.assertIn("Responses", response_content)

    def test_app(self):
        self.assertEqual(HomeConfig.name, 'home')
        self.assertEqual(apps.get_app_config('home').name, 'home')

    def test_confirmation_page_contains_content(self):
        response = Client().get('/confirmation/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Submit?", response_content)

    def test_model_can_create_new_user_and_return_attribute(self):
        new_user = User.objects.create(name='Bambang', status='Single')
        count_all_new_user = User.objects.all().count()
        self.assertEqual(count_all_new_user,1)
        self.assertEqual(str(new_user), new_user.name)

    def test_user_form_validation_for_blank(self):
        new_user = User_Input(data={'user': ''})
        self.assertFalse(new_user.is_valid())
        self.assertEqual(new_user.errors['name'], ["This field is required."])

    def test_form_validation_for_filled_items(self) :
        response = self.client.post('/confirmation/', data = {'name':'Ferdi', 'status':'Single'})
        count_all_user = User.objects.all().count()
        self.assertEqual(count_all_user, 1)

        self.assertEqual(response.status_code, 302)

        new_response = self.client.get('/')
        new_response_content = new_response.content.decode('utf-8')
        self.assertIn('Ferdi', new_response_content)
        self.assertIn('Single', new_response_content)
    
    def test_confirmation_page(self) :
        response = Client().post('', {'name': 'Ferdi', 'status': 'Single'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirmation.html')

class StoryFunctionalTest(LiveServerTestCase):
    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()

    def test_functional(self):
        self.browser.get(self.live_server_url)

        #Take content of the page
        response_content = self.browser.page_source

        #Check content of the page
        self.assertIn('Name', response_content)
        self.assertIn('Status', response_content)

        #Input and submit
        self.browser.find_element_by_name('name').send_keys('Ferdi')
        self.browser.find_element_by_name('status').send_keys('Single')
        
        #Take value in responses
        name = self.browser.find_element_by_id('inputName').get_attribute('value')
        status = self.browser.find_element_by_id('inputStatus').get_attribute('value')
        
        #Check User Input
        self.assertIn('Ferdi', name)
        self.assertIn('Single', status) 
        self.browser.find_element_by_name('submit-button').click()
        
        #Confirm the input
        self.browser.find_element_by_name('confirmation').click()








